import { TailorStorePage } from './app.po';

describe('tailor-store App', function() {
  let page: TailorStorePage;

  beforeEach(() => {
    page = new TailorStorePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
