import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

// import {TestComponent} from './test/test.component';
import {CustomizeShirtComponent} from './customize-shirt/customize-shirt.component';


const appRoutes: Routes = [
  // {
  //   path: 'test',
  //   component: TestComponent,
  //   data: {title: 'Heroes List'}
  // },
  {
    path: 'customize/shirt',
    component: CustomizeShirtComponent,
    data: {title: 'Customize Shirt'}
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: true, // <-- debugging purposes only
      }
    )
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule {
}
