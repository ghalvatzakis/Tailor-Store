import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';

import {CustomizeShirt} from './customize-shirt';

@Component({
  selector: 'app-customize-shirt',
  templateUrl: './customize-shirt.component.html',
  styleUrls: ['./customize-shirt.component.css']
})
export class CustomizeShirtComponent {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  selectedFabric: CustomizeShirt;
  selectedCollar: CustomizeShirt;
  srcValue: string;

  fabrics = [
    {id: 0, name: '01_do_1', image_src: '01_do_1.jpg'},
    {id: 1, name: '01_birdsey_black', image_src: '01_birdsey_black.jpg'},
    {id: 2, name: '01_Chain-1', image_src: '01_Chain-1.jpg'},
    {id: 3, name: '01_Check-1', image_src: '01_Check-1.jpg'},
    {id: 4, name: '01_Cotto_Black', image_src: '01_Cotto_Black.jpg'},
    {id: 5, name: '01_Diamond-1', image_src: '01_Diamond-1.jpg'},
    {id: 6, name: '01_Gingham-1', image_src: '01_Gingham-1.jpg'},
    {id: 7, name: '01_Hairline-1', image_src: '01_Hairline-1.jpg'},
    {id: 8, name: '01_Herringbone-1', image_src: '01_Herringbone-1.jpg'},
    {id: 9, name: '01_HOUNDSTOOT_01', image_src: '01_HOUNDSTOOT_01.jpg'},
    {id: 10, name: '01_nailhead-1', image_src: '01_nailhead-1.jpg'},
    {id: 11, name: '01_oxfor_ligh_blue-01', image_src: '01_oxfor_ligh_blue-01.jpg'},
    {id: 12, name: '01_Pi_check-1', image_src: '01_Pi_check-1.jpg'},
    {id: 13, name: '01_Sel_Check-01', image_src: '01_Sel_Check-01.jpg'},
    {id: 14, name: '01_shepherds-1', image_src: '01_shepherds-1.jpg'},
    {id: 15, name: '01_Stripe-1', image_src: '01_Stripe-1.jpg'},
    // '01_Swis_Dot-1.jpg',
    // '01_Whit_Premium-1.jpg',
    // '01_windowpane-1.jpg',
    // '02_ Do_2.jpg',
    // '02_ shepherds-2.jpg',
    // '02_Birdsey_blue.jpg',
    // '02_Chain-2.jpg',
    // '02_Check-2.jpg',
    // '02_Cotto_Brown.jpg',
    // '02_Diamond-2.jpg',
    // '02_Gingham-2.jpg',
    // '02_Hairline-2.jpg',
    // '02_Herringbone-2.jpg',
    // '02_HOUNDSTOOT_02.jpg',
    // '02_nailhead-2.jpg',
    // '02_Oxfor_Mediu_Blue-02.jpg',
    // '02_Pi_check-2.jpg',
    // '02_sel_check-02.jpg',
    // '02_Stripe-2.jpg',
    // '02_Swis_Dot-2.jpg',
    // '02_Whit_Premium-2.jpg',
    // '02_windowpane-2.jpg',
    // '03_ Do_3.jpg',
    // '03_ shepherds-3.jpg',
    // '03_Birdsey_Burgundy.jpg',
    // '03_Chain-3.jpg',
    // '03_Check-3.jpg',
    // '03_Cotto_burgundy.jpg',
    // '03_Diamond-3.jpg',
    // '03_Hairline-3.jpg',
    // '03_Herringbone-3.jpg',
    // '03_HOUNDSTOOT_03.jpg',
    // '03_nailhead-3.jpg',
    // '03_Oxfor_Ligh_Grey-03.jpg',
    // '03_Pi_check-3.jpg',
    // '03_sel_check_03.jpg',
    // '03_Stripe-3.jpg',
    // '03_Swis_dot-3.jpg',
    // '03_Whit_Premium-3.jpg',
    // '03_windowpane-3.jpg',
    // '04_ Do_4.jpg',
    // '04_ shepherds-4.jpg',
    // '04_Birdsey_ligh_blue.jpg',
    // '04_Chain-4.jpg',
    // '04_Check-4.jpg',
    // '04_Cotto_Cyan.jpg',
    // '04_Diamond-4.jpg',
    // '04_Gingham-4.jpg',
    // '04_Hairline-4.jpg',
    // '04_Herringbone-4.jpg',
    // '04_HOUNDSTOOT_04.jpg',
    // '04_nailhead-4.jpg',
    // '04_Oxfor_Pink-04.jpg',
    // '04_Pi_check-4.jpg',
    // '04_sel_check-04.jpg',
    // '04_Stripe-4.jpg',
    // '04_Swis_dot-4.jpg',
    // '04_Whit_Premium-4.jpg',
    // '04_windowpane-4.jpg',
    // '5_ Do_5.jpg',
    // '05_Birdsey_Midnigh_Blue.jpg',
    // '05_Chain-5.jpg',
    // '05_Check-5.jpg',
    // '05_Cotto_Green.jpg',
    // '05_Diamon-5.jpg',
    // '05_Gingham-5.jpg',
    // '05_Hairline-5.jpg',
    // '05_Herringbone-5.jpg',
    // '05_HOUNDSTOOT_05.jpg',
    // '05_nailhead-5.jpg',
    // '05_Oxfor_mediu_grey-05.jpg',
    // '05_Pi_check-5.jpg',
    // '05_sel_check-05.jpg',
    // '05_Stripe-5.jpg',
    // '05_Swis_dot-5.jpg',
    // '05_Whit_Premium-5.jpg',
    // '05_windowpane-5.jpg',
    // '06_CHECK-6.jpg',
    // '06_Cotto_Grey.jpg',
    // '06_Gingham-6.jpg',
    // '06_Hairline-6.jpg',
    // '06_nailhead-6.jpg',
    // '06_Oxfr_Ho_Pink-06.jpg',
    // '06_whit_premium-6.jpg',
    // '06_Windowpan_-6.jpg',
    // '07_check-7.jpg',
    // '07_Cotto_Ho_Pink.jpg',
    // '07_Gingham-7.jpg',
    // '07_Hairline-7.jpg',
    // '07_Ligh_purple-07.jpg',
    // '07_nailhead-7.jpg',
    // '07_Stripe-7.jpg',
    // '07_whit_premium-7.jpg',
    // '08_check-8.jpg',
    // '08_Cotto_Pink.jpg',
    // '08_Gingham-8.jpg',
    // '08_Hairline-8.jpg',
    // '08_ligh_Burgundy-08.jpg',
    // '08_nailhead-8.jpg',
    // '08_whit_premium-7.jpg',
    // '09_check-9.jpg',
    // '09_Cotto_Purple.jpg',
    // '09_Hairline-9.jpg',
    // '09_nailhead-9.jpg',
    // '09_oxfor_cream-09.jpg',
    // '09_Stripe-9.jpg',
    // '09_whit_premium-9.jpg',
    // '10_check-10.jpg',
    // '10_Cotto_Red.jpg',
    // '10_Gingham-10.jpg',
    // '10_Hairline-10.jpg',
    // '10_nailhead-10.jpg',
    // '10_oxfor_nav_blue-10.jpg',
    // '10_whit_premium-10.jpg',
    // '11_Cotto_Yello_Cotton.jpg',
    // '11_Gingham-11.jpg',
    // '11_nailhead-11.jpg',
    // '11_Stripe-11.jpg',
    // '12_Gingham-12.jpg',
    // '12_Ligh_Blue.jpg',
    // '12_nailhead-12.jpg',
    // '12_Stripe-12.jpg',
    // '13_Gingham-13.jpg',
    // '13_Mediu_Blue.jpg',
    // '13_nailhead-13.jpg',
    // '13_Stripe-13.jpg',
    // '14_Gingham-14.jpg',
    // '14_Midnigh_blue.jpg',
    // '14_nailhead-14.jpg',
    // '14_Stripe-14.jpg',
    // '15_Gingham-15.jpg',
    // '15_nailhead-15.jpg',
    // '15_Soli_White.jpg',
    // '15_Stripe-15.jpg',
    // '16_ Gingham-16.jpg',
    // '16_Stripe-16.jpg',
    // '17_Gingham-17.jpg',
    // '17_Stripe-17.jpg',
    // '18_Gingham-18.jpg',
    // '18_Stripe-18.jpg',
    // '19_ Gingham-19.jpg',
    // '19_Stripe-19.jpg',
    // '20_Gingham-20.jpg',
    // '20_Stripe-20.jpg',
    // '21_ Gingham-21.jpg',
    // '21_Stripe-21.jpg',
    // '22_ Gingham-22.jpg',
    // '22_Stripe-22.jpg',
    // '24_Stripe-24.jpg',
    // '25_Stripe-25.jpg',
    // '26_Stripe-26.jpg',
    // '28_Stripe-28.jpg',
    // '29_Stripe-29.jpg',
    // '30_Stripe-30.jpg',
    // '31_Stripe-31.jpg',
    // '32_Stripe-32.jpg',
    // '33_Stripe-33.jpg',
    // '34_Stripe-34.jpg',
    // '35_Stripe-35.jpg',
    // '36_Stripe-36.jpg',
    // '37_Stripe-37.jpg',
    // '38_Stripe-38.jpg',
    // '39_Stripe-39.jpg',
    // '40_Stripe-40.jpg',
    // '41_Stripe-41.jpg',
    // '42_Stripe-42.jpg',
    // '43_Stripe-43.jpg',
    // '44_Stripe-44.jpg',
    // '45_Stripe-45.jpg',
    // 'collar-stays.jpg',
    // 'collarback.jpg',
    // 'collarpoint.jpg',
    // 'cstitch2.jpg',
  ];
  collars = [
    {id: 1, name: '01_collar__english_cut_away', image_src: '01_collar__english_cut_away.jpg'},
    {id: 2, name: '02_collar__traditional_spread', image_src: '02_collar__traditional_spread.jpg'},
    {id: 3, name: '03_collar__classic_spread', image_src: '03_collar__classic_spread.jpg'},
    {id: 4, name: '04_collar__hidden_button_down', image_src: '04_collar__hidden_button_down.jpg'},
    {id: 5, name: '05_collar__button_tab', image_src: '05_collar__button_tab.jpg'},
    {id: 6, name: '06_collar__Groom', image_src: '06_collar__Groom.jpg'},
    {id: 7, name: '07_collar__wide_spread', image_src: '07_collar__wide_spread.jpg'},
    {id: 8, name: '08_collar__traditional-straight', image_src: '08_collar__traditional-straight.jpg'},
    {id: 9, name: '09_collar__traditional_button_down', image_src: '09_collar__traditional_button_down.jpg'},
  ];

  constructor(private _formBuilder: FormBuilder, private _sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });

  }

  selectFabric(fabric) {
    this.selectedFabric = fabric;
    console.log(fabric);
  }

  selectCollar(collar) {
    this.selectedCollar = collar;
    console.log(collar);
  }

  getSelectedFabricStyle(fabric) {
    if (this.selectedFabric && fabric.id === this.selectedFabric.id) {
      return '5px solid red';
    } else {
      return '';
    }
  }
  getSelectedCollarStyle(collar) {
    if (this.selectedCollar && collar.id === this.selectedCollar.id) {
      return '5px solid red';
    } else {
      return '';
    }
  }
}
